/**
 * @author nbooo
 * @date 22/04/16
 */
package {
import com.nbooo.layout.impl.builder.Node;

import flash.display.Sprite;
import flash.errors.IllegalOperationError;

import flexunit.framework.Test;

import org.flexunit.assertThat;

public class NodeTest {

    [Before]
    public function setUp():void {

    }

    [After]
    public function tearDown():void {

    }

    [Test]
    public function testLeaf():void {

    }

    [Test]
    public function testContainerType():void {
        var n:Node = Node.createRoot(Node.VERTICAL);
        assertThat(n.isVertical == true);
        assertThat(n.isHorizontal == false);
        n = Node.createRoot(Node.HORIZONTAL);
        assertThat(n.isVertical == false);
        assertThat(n.isHorizontal == true);
    }

    [Test]
    public function testNumberOfLeafs():void {
        var tree:Node = generateTree();
        var numberOfLeafs:int = tree.numberOfLeafs;
        assertThat(numberOfLeafs == 6);
    }


    [Test]
    public function testLabelSwap():void {
        var root:Node = Node.createRoot();
        var left:Node = Node.createContainer(root, Node.VERTICAL);
        var right:Node = Node.createContainer(root, Node.HORIZONTAL);
        assertThat(right.isHorizontal);
        assertThat(left.isVertical);
        right.swapLabels(left);
        assertThat(right.isVertical);
        assertThat(left.isHorizontal);
    }


    [Test]
    public function testApplyMethod():void {
        
    }

    [Test (expects="flash.errors.IllegalOperationError")]
    public function testMoreThanTwoNodesAttached():void {
        var n:Node = Node.createRoot();
        Node.createContainer(n);
        Node.createContainer(n);
        Node.createContainer(n);
    }


    [Test]
    public function testGetAllSubTrees():void {
        var tree:Node = generateTree();
        var subtrees:Vector.<Node> = tree.getAllSubTrees();
        assertThat(subtrees.length != 0)
    }
    
    private function generateTree():Node {
        var root:Node = Node.createRoot();
        var n1:Node = Node.createContainer(root);

        var n2:Node = Node.createContainer(n1);
        var n3:Node = Node.createContainer(n1);
        var n4:Node = Node.createContainer(n3);
        var n5:Node = Node.createContainer(n3);

        var l1:Node = Node.createLeaf(n2, new Mock(), "l1");
        var l2:Node = Node.createLeaf(n2, new Mock(), "l2");
        var l3:Node = Node.createLeaf(n4, new Mock(), "l3");
        var l4:Node = Node.createLeaf(n4, new Mock(), "l4");
        var l5:Node = Node.createLeaf(n5, new Mock(), "l5");
        var l6:Node = Node.createLeaf(n5, new Mock(), "l6");
        return root;
    }
    
}
}

import com.nbooo.layout.api.data.IResizable;

import flash.display.Sprite;

class Mock extends Sprite implements IResizable {}