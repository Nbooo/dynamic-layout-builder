/**
 * @author nbooo
 * @date 21/04/16
 */
package {
import com.nbooo.layout.api.builder.ILayout;
import com.nbooo.layout.api.builder.ILayoutBuilder;
import com.nbooo.layout.api.data.IResizable;
import com.nbooo.layout.impl.builder.GeneticLayoutBuilder;

import org.flexunit.assertThat;

public class GeneticLayoutTest {
    private var layoutBuilder:ILayoutBuilder;
    
    [Before]
    public function setUp():void {
        layoutBuilder = new GeneticLayoutBuilder()
    }
    
    [After]
    public function tearDown():void {
        layoutBuilder = null;
    }

    [Test (description="Test that all images actually can be placed inside given canvas")]
    public function testActualSizes():void {
        var images:Vector.<IResizable> = generateImageSet(40);
        var canvas:IResizable = ResizeableMockup.create(1000, 800);
        var S:Number = 1000*800;
        var SofImgBefore:Number = 0;
        for each(var img:IResizable in images)
            SofImgBefore += img.width * img.height;
        var l:ILayout = layoutBuilder.withBounds(canvas).withImages(images).build();
        l.arrange()
        var SofImgAfter:Number = 0;
        for each(var img:IResizable in images)
            SofImgAfter += img.width * img.height;
        assertThat(SofImgBefore >= S);
        assertThat(SofImgAfter <= S);
    }

    [Test]
    public function testNormalizeDesired():void {
        var sizes:Array = [1, 1, 1, 5];
        var L:ILayout = layoutBuilder.withDesiredSizes(Vector.<int>(sizes)).build();
        var nor:Number = L.normalizeDesired(5);
        assertThat(nor == .625);
    }

    [Ignore]
    [Test (description="Test large number of populations and generations")]
    public function testLargePopAndGen():void {
        var images:Vector.<IResizable> = generateImageSet(30);
        var canvas:IResizable = ResizeableMockup.create(1000, 800);
        var l:ILayout = layoutBuilder.withBounds(canvas).withGenerations(300).withPopulations(1000).withPercentOfSelected(98).withImages(images).build();
        var before:Number = new Date().time;
        l.arrange();
        var after:Number = new Date().time;
        assertThat(after - before < 2 * 1000);
        assertThat(l.coverage >= 98);
    }

    [Test (description="Test fairly small number of populations and generations")]
    public function testFewGenerations():void {
        var images:Vector.<IResizable> = generateImageSet(30);
        var canvas:IResizable = ResizeableMockup.create(1000, 800);
        var l:ILayout = layoutBuilder.withBounds(canvas).withGenerations(50).withPopulations(100).withPercentOfSelected(98).withImages(images).build();
        var before:Number = new Date().time;
        l.arrange();
        var after:Number = new Date().time;
        assertThat(after - before < 2 * 1000);
        assertThat(l.coverage >= 98);
    }

    [Test (description="Test that coverage of canvas after arrangment is at least equal to percentOfSelected")]
    public function testCanvasCoverage():void {
        var minCoverage:Number = 95;
        var numImages:int = 10;
        
        var l:ILayout = layoutBuilder.withBounds(ResizeableMockup.create(800, 600))
                                    .withImages(generateImageSet(numImages))
                                    .withPercentOfSelected(minCoverage)
                                    .withGenerations(10)
                                    .withPopulations(300)
                                    .build();
        l.arrange();
        trace("Test::testCanvasCoverage " + l);
        assertThat(l.coverage >= minCoverage);
    }

 
    private function generateImageSet(numImages:int):Vector.<IResizable> {
        var r:Vector.<IResizable> = new Vector.<IResizable>(numImages, true);
        for (var i:int = 0; i < numImages; i++)
            r[i] = (ResizeableMockup.createRandom());
        return r;
    }
}
}

import com.nbooo.layout.api.data.IResizable;

class ResizeableMockup implements IResizable {

    public static function create(w:Number = 0, h:Number = 0):IResizable {
        return new ResizeableMockup(w, h);
    }

    public static function createRandom(min:Number = 200, max:Number = 600):IResizable {
        var w:Number = Math.random() * max;
        var h:Number = Math.random() * max;
        w = w >= min ? w : min;
        h = h >= min ? h : min;
        return new ResizeableMockup(w, h);
    }

    private var w:Number = 0;
    private var h:Number = 0;
    private var px:Number = 0;
    private var py:Number = 0;

    public function ResizeableMockup(w:Number, h:Number) {
        width = w;
        height = h;
    }

    public function get width():Number {
        return w;
    }

    public function get height():Number {
        return h;
    }

    public function set width(value:Number):void {
        w = value;
    }

    public function set height(value:Number):void {
        h = value;
    }

    public function set x(value:Number):void {
        px = value;
    }

    public function set y(value:Number):void {
        py = value;
    }

    public function get y():Number {
        return py;
    }

    public function get x():Number {
        return px;
    }

    public function clone():IResizable {
        return create(width, height)
    }

    public function get desired():int { return 0;}
}