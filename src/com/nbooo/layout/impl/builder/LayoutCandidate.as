/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.impl.builder {
import com.nbooo.layout.api.data.ILayoutSettings;

internal class LayoutCandidate {
    private var mCost:Number;
    private var mTree:Node;
    private var mSettings:ILayoutSettings;

    public function LayoutCandidate(lock:TLock) {}

    public function set cost(value:Number):void {
        mCost = value;
    }

    public function get cost():Number {
        return mCost;
    }
    public function get tree():Node {
        return mTree;
    }

    public function apply(settings:ILayoutSettings):void {
        mSettings = settings ? settings : new DefaultSettings();
        scaleAndPositionLeafs(mTree, 0, 0);
    }

    private function scaleAndPositionLeafs(node:Node, topLeftX:Number, topLeftY:Number):void {
        if (!node)
            return;
        if (node.isHorizontal && node.left) {
            scaleAndPositionLeafs(node.left, topLeftX, topLeftY);
            scaleAndPositionLeafs(node.right, topLeftX + node.left.width, topLeftY);
        } else if (node.isVertical && node.left){
            scaleAndPositionLeafs(node.left, topLeftX, topLeftY);
            scaleAndPositionLeafs(node.right, topLeftX, topLeftY + node.left.height)
        } else if(node.isLeaf) {
            node.scaleAndPosition(topLeftX, topLeftY, mSettings.spacing);
        }
    }

    public static function create(tree:Node, cost:Number):LayoutCandidate {
        var c:LayoutCandidate = new LayoutCandidate(new TLock());
        c.mCost = cost;
        c.mTree = tree;
        return c;
    }

}
}

import com.nbooo.layout.api.data.ILayoutSettings;

internal class TLock{};

internal class DefaultSettings implements ILayoutSettings {
    public function get spacing():int {return 1;}
}