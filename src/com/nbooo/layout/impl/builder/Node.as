/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.impl.builder {
import avmplus.variableXml;

import com.nbooo.layout.api.data.IResizable;

import flash.errors.IllegalOperationError;
import flash.net.registerClassAlias;

import org.hamcrest.core.not;

public class Node {
    private var mContent:IResizable = null;
    private var mParent:Node = null;
    private var mLeft:Node = null;
    private var mRight:Node = null;
    private var mLabel:String = null;
    private var mRatio:Number;
    private var mWidth:Number;
    private var mHeight:Number;
    private var mType:int = 0;

    public static const VERTICAL:String = "V";
    public static const HORIZONTAL:String = "H";

    public static const TYPE_LEAF:int = 1;
    public static const TYPE_INTERNAL:int = 2;

    public static function randomType():int {
        return (Math.random() < 0.5 ) ? TYPE_INTERNAL : TYPE_LEAF;
    }

    public static function createRoot(label:String = null):Node {
        var n:Node = new Node();
        if (label)
            n.mLabel = label;
        else
            n.mLabel = Math.random() < 0.5 ? VERTICAL : HORIZONTAL;
        n.mType = TYPE_INTERNAL;
        return n;
    }

    public static function createContainer(parent:Node, label:String = null):Node {
        var n:Node = new Node();
        n.mParent = parent;
        if (!parent.mLeft)
            parent.mLeft = n;
        else if (!parent.mRight)
            parent.mRight = n;
        else
            throw new IllegalOperationError("Can't attach more children to current node, it already has two.")
        if (label)
            n.mLabel = label;
        else
            n.mLabel = Math.random() < 0.5 ? VERTICAL : HORIZONTAL;
        n.mType = TYPE_INTERNAL;
        return n;
    }

    public static function createLeaf(parent:Node, content:IResizable, label:String, d:Number):Node {
        var n:Node = new Node();
        n.mDesired = d;
        n.mParent = parent;
        if (!parent.mLeft)
            parent.mLeft = n;
        else if (!parent.mRight)
            parent.mRight = n;
        else
            throw new IllegalOperationError("Can't attach more children to current node, it already has two.")
        if (label)
            n.mLabel = label;
        n.mLabel = label;
        n.mContent = content;
        n.mType = TYPE_LEAF;
        return n;
    }

    public function  Node() {}

    public function computeRatio():Number {
        if (!mContent)
            return 0;
        mRatio = mContent.width / mContent.height;
        return mRatio;
    }
    
    public function get isVertical():Boolean {
        return mLabel == VERTICAL;
    }

    public function get isHorizontal():Boolean {
        return mLabel == HORIZONTAL;
    }
    
    public function get isRoot():Boolean {
        return mParent == null;
    }

    public function get isLeaf():Boolean {
        return mParent != null && mLeft == null && mRight == null;
    }

    public function get right():Node {
        return mRight;
    }

    public function get left():Node {
        return mLeft;
    }

    public function set left(value:Node):void {
        mLeft = value.copy();
        mLeft.mParent = this;
    }

    public function set right(value:Node):void {
        mRight = value.copy();
        mRight.mParent = this;
    }

    public function get label():String {
        return mLabel;
    }

    public function scaleAndPosition(px:Number, py:Number, spacing:int):void {
        if (!isLeaf)
            return;
        if (!mContent)
            return;
        mContent.x = px + spacing;
        mContent.y = py + spacing;
        mContent.width = mWidth - spacing  * 2;
        mContent.height = mHeight - spacing * 2;
    }

    public function swapLabels(that:Node):void {
        var tmp:String = that.label;
        that.mLabel = this.mLabel;
        this.mLabel = tmp;
    }

    public function get ratio():Number {
        return mRatio;
    }

    public function set ratio(value:Number):void {
        mRatio = value;
    }

    public function set width(value:Number):void {
        mWidth = value;
    }

    public function get width():Number {
        return mWidth;
    }

    public function set height(value:Number):void {
        mHeight = value;
    }

    public function get height():Number {
        return mHeight;
    }

    public function get type():int {
        return mType;
    }

    private var mDesired:Number = 0;
    public function get desired():Number{
        if (!isLeaf)
            return 0;
        return mDesired;
    }

    // TODO: cache numOfLeafs to reduce number of recursive calls
    public function get numberOfLeafs():int {
        if (isLeaf)
            return 1;
        if (left && right)
            return left.numberOfLeafs + right.numberOfLeafs;
        if (left)
            return left.numberOfLeafs;
        else
            return right.numberOfLeafs;
    }

    public function leafs():Vector.<Node> {
        var r:Vector.<Node> = new Vector.<Node>();
        doCollectLeafs(this, r);
        return r;
    }

    public function substitute(that:Node):void {
        if (mParent) {
            that.mParent = mParent;
            if (mParent.left == this)
                mParent.left = that;
            if (mParent.right == this)
                mParent.right = that;
        }
    }

    public function copy():Node {
        var node:Node = new Node();
        node.mLabel = this.mLabel;
        node.mContent = this.mContent;
        node.mHeight = this.mHeight;
        node.mRatio = this.mRatio;
        node.mType = this.mType;
        node.mDesired = this.mDesired;
        if (this.mLeft)
        {
            node.mLeft = mLeft.copy();
            node.mLeft.mParent = node;
        }
        if (this.mRight)
        {
            node.mRight = mRight.copy();
            node.mRight.mParent = node;
        }
        return node;
    }

    private function doCollectLeafs(node:Node, leafs:Vector.<Node>):void {
        if (!node) return;
        if (node.isLeaf) {
            leafs.push(node);
        } else {
            doCollectLeafs(node.left, leafs);
            doCollectLeafs(node.right, leafs);
        }

    }

    public function getAllSubTrees(minLeafs:int = 3):Vector.<Node> {
        var result:Vector.<Node> = new Vector.<Node>();
        generateSubtrees(this, result, minLeafs);
        return result;
    }
    private function generateSubtrees(node:Node, result:Vector.<Node>, minLeafs:int):void {
        if (!node) return;
        if (node.numberOfLeafs < minLeafs) return;
        if (!node.isRoot)
            result.push(node);
        generateSubtrees(node.left, result, minLeafs);
        generateSubtrees(node.right, result, minLeafs);
    }
}
}
