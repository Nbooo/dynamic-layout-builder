/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.impl.builder {
import com.nbooo.layout.api.builder.ILayout;
import com.nbooo.layout.api.data.ILayoutSettings;
import com.nbooo.layout.api.data.IResizable;
import flash.errors.IllegalOperationError;
import flash.utils.Dictionary;
import org.osflash.signals.Signal;

internal class GeneticLayout implements ILayout {
    
    public var percentOfSelected:Number = 0;
    public var canvas:IResizable = null;
    public var images:Vector.<IResizable> = null;
    public var desiredSizes:Vector.<int> = null;
    public var settings:ILayoutSettings = null;
    public var generations:int = 0;
    public var populations:int = 0;

    private var mCoverage:Number = 98;
    private var mSignalComplete:Signal;
    private const PLAGUE_THRESHOLD:int = 10;
    public function GeneticLayout() {
        mSignalComplete = new Signal();
    }

    public function get signalComplete():Signal {
        return mSignalComplete;
    }

    public function get coverage():Number {
        return mCoverage;
    }

    public function arrange():void {
        if (!canvas)
            throw new IllegalOperationError("No bounds! Impossible to complete the task!");
        if (!images || !images.length)
            throw new IllegalOperationError("No images to arrange!");

        var candidates:Vector.<LayoutCandidate> = new Vector.<LayoutCandidate>();
        var S:Number = canvas.width * canvas.height;

        for (var i:int = 0; i < populations; i++) {
            var internalNodes:Vector.<Node> = new Vector.<Node>(images.length - 1, true);
            var leafNodes:Vector.<Node> = new Vector.<Node>(images.length, true);
            var mRoot:Node = null;

            createTree(mRoot = Node.createRoot(), images, internalNodes, leafNodes);
            makeLayout(mRoot);
            candidates[i] = LayoutCandidate.create(mRoot, computeCost(leafNodes, S));
        }
        candidates.sort(sortByCost);

        var tree1:Node = null;
        var tree2:Node = null;
        var noChangeCounter:int = 0;
        var wasChanged:Boolean = false;
        var bestCost:Number = Number.MAX_VALUE;
        for (var i:int = 0; i < generations; i++) {
            wasChanged = false;
            var size:int = candidates.length * percentOfSelected / 100;
            var selected:Vector.<LayoutCandidate> = new Vector.<LayoutCandidate>(size, true);
            for (var n:int = 0; n < size; n++) {
                selected[n] = LayoutCandidate.create(candidates[n].tree.copy(), candidates[n].cost);
            }
            for (var j:int = 0, k:int = 1; j < selected.length - 1, k < selected.length; j+=2, k+=2){
                tree1 = selected[j].tree;
                tree2 = selected[k].tree;
                mutate(tree1);
                mutate(tree2);
                crossover(tree1, tree2)
                makeLayout(tree1);
                makeLayout(tree2);
                selected[j].cost = computeCost(getNodesByType(tree1, Node.TYPE_LEAF), S);
                selected[k].cost = computeCost(getNodesByType(tree2, Node.TYPE_LEAF), S);
            }
            candidates = candidates.concat(selected);
            candidates.sort(sortByCost);
            candidates.splice(candidates.length - size, size - 1);
            if (Number(candidates[0].cost.toFixed(6)) < Number(bestCost.toFixed(6))) {
                bestCost = candidates[0].cost;
                wasChanged = true;
            }
            if (!wasChanged)
                noChangeCounter++;
            if (noChangeCounter == PLAGUE_THRESHOLD){
                plague(candidates);
                noChangeCounter = 0;
                candidates.sort(sortByCost);
            }

        }
        candidates.sort(sortByCost);
        var result:LayoutCandidate = candidates[0];
        candidates = null;
        result.apply(settings);
        mCoverage = (1 - result.cost) * 100;
        mSignalComplete.dispatch(this);
    }

    private function plague(candidates:Vector.<LayoutCandidate>):void {
        var deathPercent:int = 90;
        var size:int = candidates.length * deathPercent / 100;
        var S:Number = canvas.width * canvas.height;
        candidates.splice(candidates.length - size, size - 1);
        for (var i:int = 0; i < size; i++) {
            var internalNodes:Vector.<Node> = new Vector.<Node>(images.length - 1, true);
            var leafNodes:Vector.<Node> = new Vector.<Node>(images.length, true);
            var mRoot:Node = null;

            createTree(mRoot = Node.createRoot(), images, internalNodes, leafNodes);
            makeLayout(mRoot);
            candidates.push(LayoutCandidate.create(mRoot, computeCost(leafNodes, S)));
        }
        candidates.sort(sortByCost);
    }

    private function createTree(root:Node, content:Vector.<IResizable>, internalNodes:Vector.<Node>, leafNodes:Vector.<Node>):void {
        internalNodes[0] = root;
        for (var i:int = 1; i < internalNodes.length; i++) {
            var parent:Node = pickRandom(internalNodes.filter(excludeFullNodes))
            internalNodes[i] = Node.createContainer(parent);
        }

        for (i = 0; i < leafNodes.length; i++){
            var n:Node = pickRandom(internalNodes.filter(excludeFullNodes));
            var normalDesiredSize:Number = desiredSizes ? normalizeDesired(desiredSizes[i]) : 0;
            var leaf:Node = Node.createLeaf(n, content[i], "P" + i, normalDesiredSize);
            leafNodes[i] = leaf;
        }
    }

    private function makeLayout(root:Node):void {
        var ar:Number = computeAspectRatio(root);
        var rootWidth:Number;
        var rootHeight:Number;
        if (canvas.width < ar * canvas.height)
            rootWidth = canvas.width;
        else
            rootWidth = ar * canvas.height;
        rootHeight = rootWidth/ar;
        computeDimensions(root, rootWidth, rootHeight);
    }

    private function mutate(tree:Node):void {
        var typeToSelect:int = Node.randomType();

        var nodes:Vector.<Node> = getNodesByType(tree, typeToSelect);
        if (nodes.length < 2) // can't mutate if there is only one node
            return;
        var n1:Node = pickRandom(nodes);

        if (n1.isLeaf)
            nodes.splice(nodes.indexOf(n1),1);
        else if (n1.isHorizontal)
            nodes = nodes.filter(excludeHorizontal);
        else
            nodes = nodes.filter(excludeVertical);

        if (!nodes.length)
            return;
        n1.swapLabels(pickRandom(nodes));
    }

    private function crossover(tree1:Node, tree2:Node):void {
        var st1:Vector.<Node> = tree1.getAllSubTrees();
        var st2:Vector.<Node> = tree2.getAllSubTrees();
        if (!st1.length || !st2.length)
            return;
        st1.sort(sortByNumberOfLeafs);
        st2.sort(sortByNumberOfLeafs);

        var map1:Dictionary = new Dictionary();
        for each (var n:Node in st1) {
            if (!map1.hasOwnProperty(String(n.numberOfLeafs)))
                map1[n.numberOfLeafs] = new Vector.<Node>();
            (map1[n.numberOfLeafs] as Vector.<Node>).push(n);
        }

        var map2:Dictionary = new Dictionary();
        for each (var n:Node in st2) {
            if (!map1.hasOwnProperty(String(n.numberOfLeafs)))
                    continue; // if there is no such num in first map, skip;
            if (!map2.hasOwnProperty(String(n.numberOfLeafs)))
                map2[n.numberOfLeafs] = new Vector.<Node>();
            (map2[n.numberOfLeafs] as Vector.<Node>).push(n);
        }

        var keys:Array = [];
        for (var k:String in map2)
            keys.push(k);
        if (!keys.length)
            return;
        var index:int = Math.random() * keys.length;
        var key:String = keys[index];
        var n1:Node = map1[key][0];
        var n2:Node = map2[key][0];
        doCrossover(n1, n2);
    }


    private function doCrossover(t1:Node, t2:Node):void {
        var leftLeafs:Vector.<Node> = t1.leafs();
        var rightLeafs:Vector.<Node> = t2.leafs();
        var copy1:Node = t2.copy();
        var copy2:Node = t1.copy();
        applyLeafs(copy1, leftLeafs);
        applyLeafs(copy2, rightLeafs);
        t1.substitute(copy1);
        t2.substitute(copy2);
    }

    private function applyLeafs(n:Node, leafs:Vector.<Node>):void {
        if (!leafs.length)
            return;
        if (!n)
            return;
        if (n.left && n.left.isLeaf)
            n.left = leafs.shift();
        if (!leafs.length)
            return;
        if (n.right && n.right.isLeaf)
            n.right = leafs.shift();
        applyLeafs(n.left, leafs);
        applyLeafs(n.right, leafs);

    }

    private function computeAspectRatio(node:Node):Number {
        if (!node)
            return 0;
        if (node.isLeaf) // it's a leaf, return image aspect ratio
            return node.computeRatio();
        if (node.isHorizontal) {
            node.ratio = computeAspectRatio(node.left) + computeAspectRatio(node.right)
        } else {
            if (node.left && node.right)
                node.ratio = 1/(1/computeAspectRatio(node.left) + 1/computeAspectRatio(node.right));
            else if (node.left)
                node.ratio = 1/(1/computeAspectRatio(node.left));
            else
                node.ratio = 1/(1/computeAspectRatio(node.right));
        }
        return node.ratio;
    }

    private function computeDimensions(node:Node, width:Number, height:Number):void {
        if (!node)
            return;
        var w:Number = Math.min(width, node.ratio * height);
        var h:Number = w/node.ratio;
        node.width = w;
        node.height = h;
        computeDimensions(node.left, w, h);
        computeDimensions(node.right, w, h);
    }

    private function normalizedSize(n:Node, S:Number):Number {
        return (n.width * n.height) / S;
    }

    public function normalizeDesired(val:int):Number {

        if (desiredSizes && desiredSizes.length) {
            var minSize:int = int.MAX_VALUE;
            var overall:int = 0;
            for (var i:int = 0; i < desiredSizes.length; i++){
                if (desiredSizes[i] < minSize)
                    minSize = desiredSizes[i];
                overall += desiredSizes[i];
            }
            if (overall == 0)
                return 0;
            var chunk:Number = 1/overall;
            return chunk * val;
        }
        return 0;
    }

    private function computeCost(leafs:Vector.<Node>, S:Number):Number {

        var C1:Number = 0;
        var C2:Number = 0;
        var k:int = 0;
        var s:Number = 0;
        var t:Number = 0;
        for each (var leaf:Node in leafs){
            s = normalizedSize(leaf, S);
            t = leaf.desired;
            if (t)
                k = s / t < .5 ? 5 : 1;
            else
                k = 0;    
            C1 += s;
            C2 += k * Math.pow((s - t), 2);
        }
        return C2 + (.15 * (1 - C1));
    }

    private function pickRandom(vec:Vector.<Node>):Node {
        var index:int = Math.random() * vec.length;
        return vec[index];
    }

    private function sortByCost(a:LayoutCandidate , b:LayoutCandidate):int{
        if (a.cost > b.cost)
            return 1;
        if (a.cost < b.cost)
            return -1;
        return 0;
    }

    private function sortByNumberOfLeafs(a:Node , b:Node):int{
        if (a.numberOfLeafs > b.numberOfLeafs)
            return 1;
        if (a.numberOfLeafs < b.numberOfLeafs)
            return -1;
        return 0;
    }


    private function getNodesByType(tree:Node, type:int, result:Vector.<Node> = null):Vector.<Node>{
        if (!tree)
            return result;
        if (!result)
            result = new Vector.<Node>();
        if (tree.type == type)
            result.push(tree);
        getNodesByType(tree.left, type, result);
        getNodesByType(tree.right, type, result);
        return result;
    }

    // filters

    private function excludeFullNodes(n:Node, ...rest):Boolean {
        return n && !(n.left && n.right);
    }

    private function excludeVertical(n:Node, ...rest):Boolean {
        return n && n.isHorizontal;
    }

    private function excludeHorizontal(n:Node, ...rest):Boolean {
        return n && n.isVertical;
    }

    private function excludeLeafs(n:Node, ...rest):Boolean {
        return n && !n.isLeaf;
    }

    private function excludeContainers(n:Node, ...rest):Boolean {
        return n && n.isLeaf;
    }

    // overriden

    public function toString():String {
        return "GeneticLayout:: workers: false, generations: " + generations
                + ", populations: " + populations
                + ", percentOfSelected: " + percentOfSelected
                + ", realCoverage: " + mCoverage;
    }

}
}


