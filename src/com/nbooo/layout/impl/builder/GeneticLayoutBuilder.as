/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.impl.builder {
import com.nbooo.layout.api.builder.ILayout;
import com.nbooo.layout.api.builder.ILayoutBuilder;
import com.nbooo.layout.api.data.IResizable;

    public class GeneticLayoutBuilder implements ILayoutBuilder{
        private var mImages:Vector.<IResizable> = null;
        private var mDesiredSizes:Vector.<int> = null;
        private var mBounds:IResizable = null;
        private var mPercents:Number = 0;
        private var mPopulations:int = 0;
        private var mGenerations:int = 0;

        public function withDesiredSizes(sizes:Vector.<int>):ILayoutBuilder {
            this.mDesiredSizes = sizes.concat();
            return this;
        }

        public function withImages(images:Vector.<IResizable>):ILayoutBuilder {
            this.mImages = images.concat();
            return this;
        }

        public function withBounds(canvasSize:IResizable):ILayoutBuilder {
            this.mBounds = canvasSize;
            return this;
        }

        public function withPercentOfSelected(percents:int):ILayoutBuilder {
            this.mPercents = percents;
            return this;
        }

        public function withPopulations(numPopulations:int):ILayoutBuilder {
            this.mPopulations = numPopulations;
            return this;
        }

        public function withGenerations(numGenerations:int):ILayoutBuilder {
            this.mGenerations = numGenerations;
            return this;
        }
        
        public function build():ILayout {
            var layout:GeneticLayout = new GeneticLayout();
            layout.images = mImages;
            layout.desiredSizes = mDesiredSizes;
            layout.canvas = mBounds;
            layout.percentOfSelected = mPercents;
            layout.populations = mPopulations ? mPopulations : 200;
            layout.generations = mGenerations ? mGenerations : 10;
            return layout;
        }
    }
}
