/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.api.builder {
import com.nbooo.layout.api.data.IResizable;

public interface ILayoutBuilder {
		function withDesiredSizes(sizes:Vector.<int>):ILayoutBuilder;
		function withImages(images:Vector.<IResizable>):ILayoutBuilder;
		function withBounds(dimension:IResizable):ILayoutBuilder;
        function withPercentOfSelected(percents:int):ILayoutBuilder;
        function withPopulations(numPopulations:int):ILayoutBuilder;
        function withGenerations(numGenerations:int):ILayoutBuilder;
		function build():ILayout;

	}
}
