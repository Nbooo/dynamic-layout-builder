/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.api.builder {
import com.nbooo.layout.api.data.IResizable;

import org.osflash.signals.Signal;

public interface ILayout {
    function get coverage():Number;
    function get signalComplete():Signal;
    function arrange():void;
    function normalizeDesired(size:int):Number;
}
}
