/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.api.data {
public interface IPositionable {
    function set x(value:Number):void
    function set y(value:Number):void;
    function get y():Number;
    function get x():Number;
}
}
