/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.api.data {
	public interface IResizable extends IPositionable{
        function get width():Number;
        function get height():Number;
        function set width(value:Number):void;
        function set height(value:Number):void;
	}
}
