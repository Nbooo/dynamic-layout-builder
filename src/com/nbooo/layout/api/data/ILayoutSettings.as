/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.layout.api.data {
public interface ILayoutSettings {
    function get spacing():int;
}
}
