/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.examples {
import com.nbooo.layout.api.data.IResizable;

import flash.events.Event;

import flash.events.EventDispatcher;

public class ImageModel extends EventDispatcher implements IResizable{
    private var mWidth:Number = 0;
    private var mHeight:Number = 0;
    private var mX:Number = 0;
    private var mY:Number = 0;
    
    public function ImageModel() {
        
    }

    public function get width():Number {
        return mWidth;
    }

    public function get height():Number {
        return mHeight;
    }

    public function set width(value:Number):void {
        if (mWidth == value)
            return;
        mWidth = value;
        dispatchEvent(new Event(Event.CHANGE));
    }

    public function set height(value:Number):void {
        if (mHeight == value)
            return;
        mHeight = value;
        dispatchEvent(new Event(Event.CHANGE));
    }

    public function set x(value:Number):void {
        if (mX == value)
            return;
        mX = value;
        dispatchEvent(new Event(Event.CHANGE));
    }

    public function set y(value:Number):void {
        if(mY == value) 
            return;
        mY = value;        
        dispatchEvent(new Event(Event.CHANGE));
    }

    public function get y():Number {
        return mY;
    }

    public function get x():Number {
        return mX;
    }
    public function get desired():int{ return 1; }
}
}
