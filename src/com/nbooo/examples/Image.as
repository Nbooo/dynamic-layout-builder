/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.examples {
import com.nbooo.layout.api.data.IResizable;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Loader;
import flash.display.Sprite;
import flash.events.Event;

public class Image extends Sprite {
    private var mModel:ImageModel;
    private var mImage:Bitmap;

    public static function fromBitmap (bmp:Bitmap):Image {
        var img:Image = new Image();
        img.update(bmp);
        return img;
    }

    public function Image() {
        super();
        mImage = new Bitmap();
        mModel = new ImageModel();
    }

    private function update(bmp:Bitmap):void {

        var bmpd:BitmapData = new BitmapData(bmp.width, bmp.height);
        bmpd.draw(bmp);
        mImage.bitmapData = bmpd;
        addChild(mImage);
        
        mModel.width = this.width;
        mModel.height = this.height;
        mModel.x = this.x;
        mModel.y = this.y;
        mModel.addEventListener(Event.CHANGE, onModelChange);
    }

    public function get model():IResizable {
        return mModel;
    }
    
    private function onModelChange(e:Event):void {
        this.width = mModel.width;
        this.height = mModel.height;
        this.x = mModel.x;
        this.y = mModel.y;
    }
}
}
