/**
 * @author nbooo
 * @date 21/04/16
 */
package com.nbooo.examples {
import com.nbooo.layout.api.builder.ILayout;
import com.nbooo.layout.api.data.IResizable;
import com.nbooo.layout.impl.builder.GeneticLayoutBuilder;

import flash.display.Bitmap;
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;

[SWF (width=1000, height=1000)]
public class ExampleUsage extends Sprite implements IResizable {
    private var models:Vector.<IResizable>;
    private var images:Vector.<Image>;
    private var sizes:Vector.<int>;
    private var numImages:int;

    public function ExampleUsage() {
        super();
        stage.scaleMode = StageScaleMode.NO_SCALE;
        stage.align = StageAlign.TOP_LEFT;
        models = new <IResizable>[];
        images = new <Image>[];
        sizes = new <int>[];
        const service:CSimpleImageLoader = new CSimpleImageLoader();
        const resource:IImgURLResource = CImagesResource.hardcoded();
        numImages = resource.count;
        service.signalLoaded.add(onImageLoaded);
        service.loadImagesFrom(resource);
    }

    override public function get width():Number {
        return stage.stageWidth;
    }

    override public function get height():Number {
        return stage.stageHeight;
    }

    private function onImageLoaded(img:Bitmap, url:String):void {
        var image:Image = Image.fromBitmap(img);
        images.push(image);
        models.push(image.model);
        var size:int = 1;
        if (url == "https://s-media-cache-ak0.pinimg.com/736x/7f/3c/00/7f3c001d0b35bebf89f2d573e798bbba.jpg")
            size = 2;
        else if (url == "https://s-media-cache-ak0.pinimg.com/736x/79/f9/bb/79f9bbe9b9721fd26ac034f1ffb737a3.jpg")
            size = 3; // this image will take most place
        else if (url == "https://s-media-cache-ak0.pinimg.com/736x/46/5e/02/465e0242262f115198e7ec8b1c151761.jpg")
            size = 2;
        sizes.push(size);
        if (models.length == numImages)
            updateLayout();
    }

    private function updateLayout():void {
        var layout:ILayout = new GeneticLayoutBuilder()
                .withBounds(this)
                .withImages(models)
                .withGenerations(250)
                .withPopulations(1000)
                .withPercentOfSelected(2)
                .withDesiredSizes(sizes)
                .build();
        layout.signalComplete.add(onArranged);
        layout.arrange();
    }

    private function onArranged(layout:ILayout) {
       for each(var img:Image in images)
            addChild(img);
    }
}
}
