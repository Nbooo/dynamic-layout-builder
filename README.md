# README #

Straightforward implementation of a Dynamic layout building algorithm described in[ this paper ](https://www.researchgate.net/publication/233529670_Photo_Layout_with_a_Fast_Evaluation_Method_and_Genetic_Algorithm) 


Since it was a part of test assignment, I didn't do any extra optimizations. The code is not optimized and is not properly covered with tests.
Also, I didn't use Workers and as result, it runs in one thread and hits the performance badly. 

The idea is to find such scale factor for each image, that the whole given canvas would be covered. 

The result of algorithm in this implementation:

![Screenshot](http://storage5.static.itmages.ru/i/16/1212/h_1481558633_5221058_ed6c7b2887.png "The result of arranging")